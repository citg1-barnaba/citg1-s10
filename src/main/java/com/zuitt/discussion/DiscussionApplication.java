package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	// Retrieve all post
	// localhost:8080/post
	@RequestMapping(value="/posts", method=RequestMethod.GET)
	public String getPosts(){
		return "All posts retrieved.";
	}

	// Creating a new post
	// localhost:8080/posts
	// @RequestMapping(value="/posts", method=RequestMethod.POST)
	@PostMapping("/posts")
	public String createPost(){
		return "New post created.";
	}

	// Retrieving a single post
	// localhost:8080/posts/1234
	// @RequestMapping(value="/posts/{postid}", method=RequestMethod.GET)
	@GetMapping("/posts/{postid}")
	public String getPost(@PathVariable Long postid){
		return "Viewing details of post " + postid;
	}

	// Deleting a post
	// localhost:8080/posts/1234
	// @RequestMapping(value="/posts/{postid}", method=RequestMethod.DELETE)
	@DeleteMapping("/posts/{postid}")
	public String deletePost(@PathVariable Long postid){
		return "The post " + postid + " has been deleted.";
	}

	// Update a post
	// localhost:8080/posts/1234
	//@RequestMapping(value="/posts/{postid}", method=RequestMethod.PUT)
	@PutMapping("/posts/{postid}")
	// Automatically converts the format to JSON
	@ResponseBody
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
		return post;
	}

	// Retrieving a posts for a particular user
	// localhost:8080/myPost
	// @RequestMapping(value="/myPosts", method=RequestMethod.GET)
	@GetMapping("/myPosts")
	public String getMyPosts(@RequestHeader(value="Authorization") String user){
		return "Posts for " +  user + " have been retrieved";
	}

	// S10 Activity
	// Create the following routes for the user using REST API:

	// a. Get all users
	// localhost:8080/users
	@GetMapping("/users")
	public String getAllUser(){
		return "All users retrieved";
	}

	// b. Create user
	// localhost:8080/users
	@PostMapping("/users")
	public String createUser(){
		return "New user created";
	}

	// c. Get a specific user
	// localhost:8080/users/{userid}
	@GetMapping("/users/{userid}")
	public String getUser(@PathVariable Long userid){
		return "Viewing details of user " + userid;
	}

	// d. Delete a user
	// localhost:8080/users/{userid}
	@DeleteMapping("/users/{userid}")
	public String deleteUser(@PathVariable Long userid, @RequestHeader(value="Authorization") String user){
		if (user.isEmpty()){
			return "Unauthorized access";
		} else {
			return "The user " + userid + " has been deleted.";
		}
	}

	// e. Update a user
	// localhost:8080/users/{userid}
	@PutMapping("/users/{userid}")
	@ResponseBody
	public Users updateUser(@PathVariable Long userid, @RequestBody Users user){
		return user;
	}
}
